#include "animate-vertices.h"
#include <iostream>
using namespace std;

void AnimateVertices::onPluginLoad()
{
    // Carregar shader, compile & link 
    fs = new QGLShader(QGLShader::Vertex);
    fs->compileSourceFile("../shaders/hector/animate_vertices_01.vert");
    program = new QGLShaderProgram(this);
    program->addShader(fs);
    program->link();

    timer = new QElapsedTimer();
    timer->start();
}

void AnimateVertices::preFrame() 
{
    // bind shader and define uniforms
    program->bind();
    int t = (int)(timer->elapsed() / 1000);
    
    cout << t << endl;

    program->setUniformValue("time", t);
    program->setUniformValue("A", 0.1f);
    program->setUniformValue("F", 1);
}

void AnimateVertices::postFrame() 
{
    // unbind shader
    program->release();
}

Q_EXPORT_PLUGIN2(animate-vertices, AnimateVertices)
