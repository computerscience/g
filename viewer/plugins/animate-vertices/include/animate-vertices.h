#ifndef _ANIMATEVERTICES_H
#define _ANIMATEVERTICES_H

//#include "GL/glew.h"
#include <QGLShader>
#include <QGLShaderProgram>
#include <QElapsedTimer>
#include "effectinterface.h"

class AnimateVertices : public QObject, public EffectInterface
 {
     Q_OBJECT
     Q_INTERFACES(EffectInterface)

 public:
    void onPluginLoad();
    void preFrame();
    void postFrame();
    
 private:
    QGLShaderProgram* program;
    QGLShader* fs;
    QElapsedTimer* timer;
 };
 
 #endif
