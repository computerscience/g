TEMPLATE=subdirs
SUBDIRS = draw-immediate navigate-default render-default 
SUBDIRS += alpha-blending
SUBDIRS += effect-crt
SUBDIRS += show-help

# Afegiu a SUBDIRS els vostres plugins!
SUBDIRS += animate-vertices
SUBDIRS += blinn-phong
