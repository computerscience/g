#ifndef _BLINNPHONG_H
#define _BLINNPHONG_H

//#include "GL/glew.h"
#include <QGLShader>
#include <QGLShaderProgram>
#include "effectinterface.h"

class BlinnPhong : public QObject, public EffectInterface
 {
     Q_OBJECT
     Q_INTERFACES(EffectInterface)

 public:
    void onPluginLoad();
    void preFrame();
    void postFrame();
    
 private:
    QGLShaderProgram* program;
 };
 
 #endif
