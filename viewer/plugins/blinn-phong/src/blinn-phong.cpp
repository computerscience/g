#include "blinn-phong.h"
#include <iostream>
using namespace std;

void BlinnPhong::onPluginLoad()
{
    program = new QGLShaderProgram(this);

    // Carregar shader, compile & link 
    QGLShader* v = new QGLShader(QGLShader::Vertex);
    v->compileSourceFile("../shaders/hector/lighting_05.vert");
    program->addShader(v);

    QGLShader* f = new QGLShader(QGLShader::Fragment);
    f->compileSourceFile("../shaders/hector/lighting_05.frag");
    program->addShader(f);


    program->link();
}

void BlinnPhong::preFrame() 
{
    // bind shader and define uniforms
    program->bind();
}

void BlinnPhong::postFrame() 
{
    // unbind shader
    program->release();
}

Q_EXPORT_PLUGIN2(blinn-phong, BlinnPhong)
