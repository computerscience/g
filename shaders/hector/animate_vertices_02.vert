// simple vertex shader
uniform float time;
uniform float A;
uniform float F;

void main()
{
	float PI = 3.1415926;
	// Normal direction
	vec4 dir = vec4(normalize(gl_Normal), 0.0);
	// Distance
	float d = A * sin(2 * PI * (gl_MultiTexCoord0.s  + F * time));
	// Move the vertex!
	gl_Position    = gl_ModelViewProjectionMatrix * ((d * dir) + gl_Vertex);
	gl_FrontColor  = (gl_NormalMatrix * gl_Normal).z;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
