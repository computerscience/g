// simple vertex shader

varying vec4 Vobj;
varying vec3 Nobj;

void main()
{
	Vobj = gl_Vertex;
	Nobj = gl_Normal;

	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_FrontColor  = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
