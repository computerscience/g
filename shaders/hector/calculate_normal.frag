// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
varying vec4 Vobs;

void main()
{
	vec3 my_Normal = normalize(cross(dFdx(Vobs.xyz), dFdy(Vobs.xyz)));
	gl_FragColor = gl_Color * my_Normal.z;
}
