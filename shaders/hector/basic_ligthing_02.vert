// simple vertex shader

vec4 getAmbient()
{
	return gl_FrontMaterial.ambient * (gl_LightModel.ambient + gl_LightSource[0].ambient);
}

vec4 getDiffuse(vec3 N, vec3 L)
{
	return gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse * max(0.0, dot(N, L));
}

vec4 getSpecular(vec3 N, vec3 H)
{
	return gl_FrontMaterial.specular * gl_LightSource[0].specular *
		pow(max(0.0, dot(N, H)), gl_FrontMaterial.shininess);
}

void main()
{
	vec4 Vobs = gl_ModelViewMatrix * gl_Vertex;
	vec3 N = normalize(gl_NormalMatrix * gl_Normal);
	vec3 L = normalize((gl_LightSource[0].position - Vobs).xyz);
	vec3 H = normalize(vec3(0, 0, 1) + L);

	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_FrontColor  = gl_FrontMaterial.emission + getAmbient() + getDiffuse(N, L)
		+ getSpecular(N, H);
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
