// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
varying vec4 Vndc;

void main()
{
	int duration = 2;
	float xlimit = 2.0 * (time / float(duration)) - 1.0;
	if(Vndc.x < xlimit) discard;

	gl_FragColor = vec4(0, 0, 1, 1);
}
