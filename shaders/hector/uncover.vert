// simple vertex shader
varying vec4 Vndc;

void main()
{
	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	Vndc = gl_Position / gl_Position.w;
	gl_FrontColor  = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
