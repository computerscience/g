// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
varying vec4 Vobs;
varying vec3 Nobs;

vec4 getAmbient()
{
	return gl_FrontMaterial.ambient * (gl_LightModel.ambient + gl_LightSource[0].ambient);
}

vec4 getDiffuse(vec3 N, vec3 L)
{
	return gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse * max(0.0, dot(N, L));
}

vec4 getSpecular(vec3 N, vec3 H)
{
	return gl_FrontMaterial.specular * gl_LightSource[0].specular *
		pow(max(0.0, dot(N, H)), gl_FrontMaterial.shininess);
}

void main()
{
	vec3 V = vec3(0, 0, 1);
	vec3 L = normalize((gl_LightSource[0].position - Vobs).xyz);
	vec3 H = normalize(V + L);
	gl_FragColor = gl_FrontMaterial.emission + getAmbient() + getDiffuse(Nobs, L) + getSpecular(Nobs, H);
}
