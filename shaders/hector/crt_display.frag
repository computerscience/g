// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
uniform int n;

void main()
{
	if(mod(gl_FragCoord.y-0.5, n) != 0)
		discard;

	gl_FragColor = gl_Color;
}
