// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;

vec4 senyera(vec4 p)
{
	if(mod(floor(p.s * 9.0), 2.0) == 0)
		return vec4(1, 1, 0, 1);
	else
		return vec4(1, 0, 0, 1);
}

void main()
{
	gl_FragColor = senyera(gl_TexCoord[0]);
}
