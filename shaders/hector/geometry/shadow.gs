// simple geometry shader

// these lines enable the geometry shader support.
#version 330 compatibility
#extension GL_EXT_geometry_shader4 : enable

void main( void )
{
	for( int i = 0 ; i < gl_VerticesIn ; i++ )
	{
		gl_FrontColor  = gl_FrontColorIn[ i ];
		gl_Position    = gl_ProjectionMatrix * gl_PositionIn[i];
		gl_TexCoord[0] = gl_TexCoordIn  [ i ][ 0 ];
		EmitVertex();
	}
    EndPrimitive();

	for( int i = 0 ; i < gl_VerticesIn ; i++ )
	{
		gl_FrontColor  = vec4(0,0,0,0);
		gl_Position    = gl_ProjectionMatrix * (
			gl_PositionIn  [ i ] - vec4(0, gl_PositionIn[i].y + 2, 0, 0));
		gl_TexCoord[0] = gl_TexCoordIn  [ i ][ 0 ];
		EmitVertex();
	}
}
