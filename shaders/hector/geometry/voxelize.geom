// simple geometry shader

// these lines enable the geometry shader support.
#version 330 compatibility
#extension GL_EXT_geometry_shader4 : enable
uniform float stepi;

void main( void )
{
float step = stepi/2.;

    vec4 bari = (gl_PositionIn[0] + gl_PositionIn[1] +gl_PositionIn[2])/3;

	vec4 bar = floor((bari/stepi)+(0.5,0.5,0.5));
	bar = bar * stepi;
    float dist = 0;

        gl_FrontColor  = gl_FrontColorIn[ 0 ];
//-x
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y-step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y+step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y-step,bar.z-step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y+step,bar.z-step,1) ;
        EmitVertex();
EndPrimitive();

//-y
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y-step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y-step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y-step,bar.z-step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y-step,bar.z-step,1) ;
        EmitVertex();
        EndPrimitive();

//-z
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y+step,bar.z-step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y-step,bar.z-step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y+step,bar.z-step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y-step,bar.z-step,1) ;
        EmitVertex();
EndPrimitive();
//+x
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y-step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y+step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y+step,bar.z-step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y+step,bar.z-step,1) ;
        EmitVertex();
EndPrimitive();

//+z
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y+step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y-step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y+step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y-step,bar.z+step,1) ;
        EmitVertex();
EndPrimitive();

//+y
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y+step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y+step,bar.z+step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x-step,bar.y+step,bar.z-step,1) ;
        EmitVertex();
        gl_Position    = gl_ModelViewProjectionMatrix * vec4(bar.x+step,bar.y+step,bar.z-step,1) ;
        EmitVertex();
        EndPrimitive();
}
