// simple geometry shader

// these lines enable the geometry shader support.
#version 330 compatibility
#extension GL_EXT_geometry_shader4 : enable
uniform float disp;

void main( void )
{
	vec4 bar = (gl_PositionIn[0] + gl_PositionIn[1] + gl_PositionIn[2]) / 3;
	vec4 despl = (disp * normalize(vec4(cross(
		gl_PositionIn[1].xyz - gl_PositionIn[0].xyz,
		gl_PositionIn[2].xyz - gl_PositionIn[0].xyz), 0)));

	
	gl_FrontColor  = gl_FrontColorIn[ 0 ];

	gl_Position    = gl_ProjectionMatrix * gl_PositionIn  [ 0 ];
	EmitVertex();
	gl_Position    = gl_ProjectionMatrix * gl_PositionIn  [ 1 ];
	EmitVertex();

	gl_FrontColor  = vec4(1,1,1,1);
	gl_Position    = gl_ProjectionMatrix * (bar + despl);
	EmitVertex();
	EndPrimitive();

	gl_FrontColor  = gl_FrontColorIn[ 0 ];
	gl_Position    = gl_ProjectionMatrix * gl_PositionIn  [ 0 ];
	EmitVertex();
	gl_Position    = gl_ProjectionMatrix * gl_PositionIn  [ 2 ];
	EmitVertex();

	gl_FrontColor  = vec4(1,1,1,1);
	gl_Position    = gl_ProjectionMatrix * (bar + despl);
	EmitVertex();
	EndPrimitive();

	gl_FrontColor  = gl_FrontColorIn[ 0 ];
	gl_Position    = gl_ProjectionMatrix * gl_PositionIn  [ 1 ];
	EmitVertex();
	gl_Position    = gl_ProjectionMatrix * gl_PositionIn  [ 2 ];
	EmitVertex();

	gl_FrontColor  = vec4(1,1,1,1);
	gl_Position    = gl_ProjectionMatrix * (bar + despl);
	EmitVertex();
	EndPrimitive();
}
