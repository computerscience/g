// simple vertex shader
uniform float time;
uniform float speed;

vec4 rotate(vec4 v, float phi)
{
	mat3 my_RotationMatrix = mat3(
		cos(phi), 0.0, -sin(phi),
		0.0, 1.0, 0.0,
		sin(phi), 0.0, cos(phi)
	);

	return vec4(my_RotationMatrix * v.xyz, v.w);
}

void main()
{
	gl_Position    = gl_ModelViewProjectionMatrix * rotate(gl_Vertex, time * speed);
	gl_FrontColor  = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
