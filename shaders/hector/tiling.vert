// simple vertex shader
uniform int tiles;

void main()
{
	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_FrontColor  = vec4(1);
	gl_MultiTexCoord0.s *= tiles;
	gl_MultiTexCoord0.t *= tiles;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
