// simple vertex shader
uniform float minY;
uniform float maxY;

// COLOR constants
vec4 COLOR_RED = vec4(1, 0, 0, 1);
vec4 COLOR_BLUE = vec4(0, 0, 1, 1);
vec4 COLOR_GREEN = vec4(0, 1, 0, 1);
vec4 COLOR_YELLOW = vec4(1, 1, 0, 1);
vec4 COLOR_CYAN = vec4(0, 1, 1, 1);

vec4 gradient(vec4 pos)
{
	float red = minY;
	float blue = maxY;
	float green = (red + blue) / 2.0;
	float yellow = (red + green) / 2.0;
	float cyan = (green + blue) / 2.0;

	if(pos.y <= red)
		return COLOR_RED;

	if(pos.y >= blue)
		return COLOR_BLUE;

	if(pos.y <= yellow)
		return mix(COLOR_RED, COLOR_YELLOW, (pos.y - red) / (yellow - red));

	if(pos.y <= green)
		return mix(COLOR_YELLOW, COLOR_GREEN, (pos.y - yellow) / (green - yellow));

	if(pos.y <= cyan)
		return mix(COLOR_GREEN, COLOR_CYAN, (pos.y - green) / (cyan - green));

	return mix(COLOR_CYAN, COLOR_BLUE, (pos.y - cyan) / (blue - cyan));
}

void main()
{
	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_FrontColor  = gradient(gl_Position / gl_Position.w);
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
