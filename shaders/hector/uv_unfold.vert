// simple vertex shader
uniform vec2 Min;
uniform vec2 Max;

void main()
{
	vec2 my_Texture = 2.0 * (gl_MultiTexCoord0.st - Min) / (Max - Min) - 1.0;
	gl_Position    = vec4(my_Texture, 0.0, 1.0);
	gl_FrontColor  = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
