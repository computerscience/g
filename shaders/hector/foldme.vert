// simple vertex shader
uniform float time;

void main()
{
	float phi = - time * gl_MultiTexCoord0.s;
	mat3 my_RotationMatrix = mat3(
		cos(phi), 0, -sin(phi),
		0, 1, 0,
		sin(phi), 0, cos(phi)
	);
	gl_Position    = gl_ModelViewProjectionMatrix * vec4(my_RotationMatrix * gl_Vertex.xyz, 1.0);
	gl_FrontColor  = vec4(0.0, 0.0, 1.0, 1.0);
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
