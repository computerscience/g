// simple vertex shader

vec3 getAmbient()
{
	return gl_FrontMaterial.ambient.rgb * (gl_LightModel.ambient.rgb +
		gl_LightSource[0].ambient.rgb);
}

vec3 getDiffuse(vec3 N, vec3 L)
{
	
}

void main()
{
	vec3 Vobs = vec3(gl_ModelViewMatrix * gl_Vertex);
	vec3 N = normalize(gl_NormalMatrix * gl_Normal);
	vec3 L = normalize(gl_LightSource[0].position.xyz - Vobs);
	vec3 color = gl_FrontMaterial.emission.rgb + getAmbient() + getDiffuse(N, L);
	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_FrontColor  = vec4(color, 1);
	gl_TexCoord[0] = gl_MultiTexCoord0;
}

