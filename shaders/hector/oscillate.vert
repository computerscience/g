// simple vertex shader
uniform float time;

void main()
{
	float d = gl_Vertex.y * sin(time);
	vec4 displacement = vec4(d * gl_Normal, 0.0);
	gl_Position    = gl_ModelViewProjectionMatrix * (displacement + gl_Vertex);
	gl_FrontColor  = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
