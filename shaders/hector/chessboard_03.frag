// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
uniform int N;

vec4 chessboard(vec4 p)
{
	float width = 0.2;
	if(mod(p.s, 2.0) - width >= 0.0 && mod(p.t, 2.0) - width >= 0.0)
		return vec4(1, 1, 1, 1);
	else
		return vec4(0, 0, 0, 1);
}

void main()
{
	gl_FragColor = chessboard(gl_TexCoord[0] * float(N*2));
}
