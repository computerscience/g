// simple vertex shader

uniform vec2 Min,Max;

void main()
{
	vec2 coord_aux = vec2(gl_MultiTexCoord0.s, gl_MultiTexCoord0.t);
	coord_aux = 2.0*(coord_aux-Min)/(Max-Min)+vec2(-1.0,-1.0);
	gl_Position    = vec4(coord_aux,0.0,1.0);
	gl_FrontColor  = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
