// simple vertex shader

uniform float time;

void main()
{
	float s = gl_MultiTexCoord0.s;
	float phi = -time * s;
	mat4 rotacio = mat4(vec4(cos(phi),0.0,-sin(phi),0.0),
						vec4(0.0,1.0,0.0,0.0),
						vec4(sin(phi),0.0,cos(phi),0.0),
						vec4(0.0,0.0,0.0,1.0));
	gl_Position    = gl_ModelViewProjectionMatrix * rotacio * gl_Vertex;
	gl_FrontColor  = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
