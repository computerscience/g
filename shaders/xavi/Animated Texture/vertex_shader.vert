// simple vertex shader

varying vec4 Coordenades_Tex;
uniform float time;
uniform int speed;

void main()
{
	speed=100;
	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_FrontColor  = vec4((gl_NormalMatrix*gl_Normal).z);
	gl_TexCoord[0].s = gl_MultiTexCoord0.s+time*speed/1000;
	gl_TexCoord[0].t = gl_MultiTexCoord0.t;

	Coordenades_Tex=gl_MultiTexCoord0;
}
