// simple vertex shader

void main()
{
	vec3 Nobs,L,V,R;
	vec4 Vobs=gl_ModelViewMatrix*gl_Vertex;

	L = (gl_LightSource[0].position-Vobs).xyz;
	L=normalize(L);

	Nobs	= gl_NormalMatrix*gl_Normal;
	Nobs=normalize(Nobs);

	V=-(Vobs).xyz;
	V=normalize(V);
	R=2*dot(Nobs,L)*Nobs-L;

	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;

	gl_FrontColor= 	gl_FrontMaterial.emission+
					gl_FrontMaterial.ambient*(gl_LightModel.ambient+gl_LightSource[0].ambient)+
					gl_FrontMaterial.diffuse*gl_LightSource[0].diffuse*(max(0.0,dot(Nobs,L)))+
					gl_FrontMaterial.specular*gl_LightSource[0].specular*pow(max(0.0,dot(R,V)),gl_FrontMaterial.shininess);

	gl_TexCoord[0] = gl_MultiTexCoord0;
}
