// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
uniform bool world;
varying vec3 Nobs;
varying vec4 Vobs;

vec4 light(vec3 N, vec3 V, vec3 L)
{
	N=normalize(N); V=normalize(V); L=normalize(L);
	vec3 R = normalize( 2.0*dot(N,L)*N-L );
	float NdotL = max( 0.0, dot( N,L ) );
	float RdotV = max( 0.0, dot( R,V ) );
	float Idiff = NdotL;
	float Ispec = pow( RdotV, gl_FrontMaterial.shininess );
	return
		gl_FrontMaterial.emission +
		gl_FrontMaterial.ambient * gl_LightModel.ambient +
		gl_FrontMaterial.ambient * gl_LightSource[0].ambient +
		gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse * Idiff+
		gl_FrontMaterial.specular * gl_LightSource[0].specular * Ispec;
}

void main()
{
	vec3 L,V;
	Nobs=normalize(Nobs);

	if (world){
		L=((gl_ModelViewMatrixInverse*gl_LightSource[0].position)-Vobs).xyz;
		L = normalize(L);

		vec4 aux = gl_ModelViewMatrixInverse*vec4(0.0,0.0,0.0,1.0);
		V = (aux-(Vobs)).xyz;
		V = normalize(V);
	}
	else{
		L = (gl_LightSource[0].position-Vobs).xyz;
		L = normalize(L);

		V = -(Vobs).xyz;
		V = normalize(V);
	}


	gl_FragColor = light(Nobs,V,L);
}