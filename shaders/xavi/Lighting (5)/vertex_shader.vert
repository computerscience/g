// simple vertex shader

varying vec3 Nobs;
varying vec4 Vobs;
uniform bool world;

void main()
{
	if (world){
		Nobs=gl_Normal;
		Vobs=gl_Vertex;
	}
	else{
		Nobs	= gl_NormalMatrix*gl_Normal;
		Vobs	= gl_ModelViewMatrix*gl_Vertex;
	}

	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_FrontColor  = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
