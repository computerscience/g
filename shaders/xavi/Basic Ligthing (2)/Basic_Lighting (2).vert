// simple vertex shader

varying vec3 Nobs;

void main()
{
	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	Nobs	= gl_NormalMatrix*gl_Normal;
	gl_FrontColor  = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
