// simple fragment shader

varying vec3 Nobs;
// 'time' contains seconds since the program was linked.
uniform float time;

void main()
{
	gl_FragColor = gl_Color*Nobs.z;
}
