// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform sampler2D sampler;
uniform float time;
uniform int tiles;

void main() {
    gl_FragColor = gl_Color;
}
