// simple vertex shader

uniform float time;
uniform float speed;
varying vec3 Nobs;
varying vec4 Vobs;

void main()
{
    vec3 dist = -gl_Vertex.xyz;
    vec3 transf = normalize(dist);
 	gl_Position = gl_ModelViewProjectionMatrix * vec4(transf, 1.0);
    gl_FrontColor = gl_Color;
    gl_TexCoord[0] = gl_MultiTexCoord0;
}