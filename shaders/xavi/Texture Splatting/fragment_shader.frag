// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
uniform sampler2D sampler1;
uniform sampler2D sampler2;
uniform sampler2D sampler3;


void main()
{
	float interp = texture2D(sampler3,gl_TexCoord[0].st).r;
	//gl_FragColor = mix(texture2D(sampler1,gl_TexCoord[0].st),texture2D(sampler2,gl_TexCoord[0].st),interp);
	gl_FragColor = interp*texture2D(sampler1,gl_TexCoord[0].st)+(1-interp)*texture2D(sampler2,gl_TexCoord[0].st);
}
