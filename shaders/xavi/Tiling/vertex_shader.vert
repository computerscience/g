// simple vertex shader

varying vec4 Coordenades_Tex;
uniform int tiles=1;
uniform float time;
uniform bool bogeria;

void main()
{
	float PI = 3.14159265368;
	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_FrontColor  = vec4((gl_NormalMatrix*gl_Normal).z);
	if (bogeria){
		gl_TexCoord[0] = gl_MultiTexCoord0*tiles*vec4(0.5*(sin(2*PI*time)+1));
	}
	else{
		gl_TexCoord[0] = gl_MultiTexCoord0*tiles;
	}
	Coordenades_Tex=gl_MultiTexCoord0;
}
