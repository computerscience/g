// simple vertex shader

uniform float time;
uniform float speed;
varying vec3 Nobs;
varying vec4 Vobs;

void main()
{
    float fi = (-time)*speed;
    mat4 rot = mat4(vec4(cos(fi),0.0,-sin(fi),0.0),vec4(0.0,1.0,0.0,0.0),vec4(sin(fi),0.0,cos(fi),0.0),vec4(0.0,0.0,0.0,1.0));
 	gl_Position = gl_ModelViewProjectionMatrix *rot * gl_Vertex;
    gl_FrontColor = gl_Color;
    gl_TexCoord[0] = gl_MultiTexCoord0;
}