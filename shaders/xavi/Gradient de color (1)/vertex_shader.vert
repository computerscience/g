// simple vertex shader

uniform float miny;
uniform float maxy;
varying vec3 Nobs;
varying vec4 Vobs;

void main()
{
    gl_Position = gl_ModelViewProjectionMatrix*gl_Vertex;
    float yred = miny;
    float yblue = maxy;
    float ygreen = (yred+yblue)/2;
    float yyellow = (yred+ygreen)/2;
    float ycyan = (ygreen+yblue)/2;
    vec4 Color;
    float punty = gl_Vertex.y;
    if (punty >= yred && punty <= yyellow) {
        float inter = float(float(punty-yred)/float(yyellow-yred));
        Color = mix(vec4(1.0,0.0,0.0,1.0),vec4(1.0, 1.0, 0.0, 1.0), inter);
    }
    else if (punty >= yyellow && punty <= ygreen) {
        float inter = float(float(punty-yyellow))/(float(ygreen-yyellow));
        Color = mix(vec4(1.0,1.0,0.0,1.0),vec4(0.0, 1.0, 0.0, 1.0), inter);
    }
    else if (punty >= ygreen && punty <= ycyan) {
        float inter = float(float(punty-ygreen)/float(ycyan-ygreen));
        Color = mix(vec4(0.0,1.0,0.0,1.0),vec4(0.0, 1.0, 1.0, 1.0), inter);
    }
    else {
        float inter = float(float(punty-ycyan)/float(yblue-ycyan));
        Color = mix(vec4(0.0,1.0,1.0,1.0),vec4(0.0, 0.0, 1.0, 1.0), inter);
    }
    gl_FrontColor = Color;
    gl_TexCoord[0] = gl_MultiTexCoord0;
}