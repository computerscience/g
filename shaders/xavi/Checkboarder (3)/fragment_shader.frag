// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
uniform float N;

void main()
{
	float interval = 1.0/N;
	float pos_x = mod(gl_TexCoord[0].s,interval);
	float pos_y = mod(gl_TexCoord[0].t,interval);

	float thickness = 20.0;

	if (pos_x<interval/thickness || pos_x>(thickness-1.0)*interval/thickness){
		gl_FragColor = vec4(0.0,0.0,0.0,1.0);
	}
	else if (pos_y<interval/thickness || pos_y>(thickness-1.0)*interval/thickness){
		gl_FragColor = vec4(0.0,0.0,0.0,1.0);
	}
	else{
		gl_FragColor = vec4(1.0,1.0,1.0,1.0);
	}
}
