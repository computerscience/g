// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
uniform float N;

void main()
{
	float interval = 1.0/N;
	float pos_x = floor(gl_TexCoord[0].s/interval);
	float pos_y = floor(gl_TexCoord[0].t/interval);

	if (mod(pos_x+pos_y,2.0)==0.0){
		gl_FragColor = vec4(1.0,1.0,1.0,1.0);
	}
	else{
		gl_FragColor = vec4(0.0,0.0,0.0,1.0);
	}
}
