// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
uniform float N;

void main()
{
	float interval = 1.0/9.0;
	float pos_x = floor(fract(gl_TexCoord[0].s)/interval);

	if (mod(pos_x,2)==0){
		gl_FragColor = vec4(1.0,1.0,0.0,1.0);
	}
	else{
		gl_FragColor = vec4(1.0,0.0,0.0,1.0);
	}
}
