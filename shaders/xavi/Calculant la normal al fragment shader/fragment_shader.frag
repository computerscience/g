// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
varying vec4 vertexpalfragment;
varying vec4 Vobs;

vec4 light(vec3 N, vec3 V, vec3 L)
{
	N=normalize(N); L=normalize(L);
	float NdotL = max( 0.0, dot( N,L ) );
	float Idiff = NdotL;
	return gl_Color * gl_LightSource[0].diffuse * Idiff;
}

void main()
{
	vec3 L,V;
	vec3 normal = cross(dFdx(vertexpalfragment),dFdy(vertexpalfragment));
	normal=normalize(normal);

	L = (gl_LightSource[0].position-vertexpalfragment).xyz;
	L = normalize(L);

	V = -(vertexpalfragment).xyz;
	V = normalize(V);

	gl_FragColor = light(normal,V,L);
}

