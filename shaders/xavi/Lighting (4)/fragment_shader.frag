// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
varying vec3 Nobs;
varying vec4 Vobs;

void main()
{
	vec3 L,R,V;
	Nobs=normalize(Nobs);

	L 	= 	(gl_LightSource[0].position-Vobs).xyz;
	L	=	normalize(L);

	R=2*dot(Nobs,L)*Nobs-L;
	V=vec3(0.0,0.0,1.0);

	gl_FragColor = 	gl_FrontMaterial.emission+
					gl_FrontMaterial.ambient*(gl_LightModel.ambient+gl_LightSource[0].ambient)+
					gl_FrontMaterial.diffuse*gl_LightSource[0].diffuse*(max(0.0,dot(Nobs,L)))+
					gl_FrontMaterial.specular*gl_LightSource[0].specular*pow(max(0.0,dot(R,V)),gl_FrontMaterial.shininess);
}