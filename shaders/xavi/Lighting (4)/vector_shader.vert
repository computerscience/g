// simple vertex shader

varying vec3 Nobs;
varying vec4 Vobs;

void main()
{
	Nobs	= gl_NormalMatrix*gl_Normal;
	Vobs=gl_ModelViewMatrix*gl_Vertex;

	gl_Position    = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
