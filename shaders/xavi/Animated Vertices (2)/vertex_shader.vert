uniform float time;
uniform float A;

void main()
{
	float PI = 3.14159265368;
	float Freq = 2*PI;
	float fase = Freq*gl_MultiTexCoord0.s;

	vec3 aux2 = gl_Vertex.xyz + gl_Normal*(A*sin(Freq*time+fase));

	gl_Position    = gl_ModelViewProjectionMatrix * vec4(aux2,gl_Vertex.w);

	gl_FrontColor  = vec4((gl_NormalMatrix*gl_Normal).z);

	gl_TexCoord[0] = gl_MultiTexCoord0;

}

