// simple vertex shader

uniform float time;
uniform bool eyespace;
varying vec3 Nobs;
varying vec4 Vobs;

void main()
{
    float d;
    vec3 normal;
    if (eyespace) {
        vec4 novpos = gl_Vertex*gl_ModelViewMatrix;
        normal = normalize(gl_Normal*gl_NormalMatrix);
        d = sin(time*2.0*3.14159)*novpos.y;
    }
    else {
        d = sin(time*2.0*3.14159)*gl_Vertex.y; 
        normal = normalize(gl_Normal);
    }
    vec4 norm = (normal.x, normal.y, normal.z, 0.1);
    vec4 nouv = gl_Vertex+norm*d;
 	gl_Position = gl_ModelViewProjectionMatrix * nouv;
    gl_FrontColor = gl_Color;
    gl_TexCoord[0] = gl_MultiTexCoord0;
}