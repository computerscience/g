// simple fragment shader

// 'time' contains seconds since the program was linked.
uniform float time;
varying float coordinate_x;

void main()
{
	if ((coordinate_x+1.0)<time){
		gl_FragColor = vec4(0.0,0.0,1.0,1.0);
	}
	else{
		discard;
	}
}
